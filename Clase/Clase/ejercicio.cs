﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase
{
    class ejercicio
    {
        /// <summary>
        /// Este es el menu que utilizaremos para archivar todos los ejercicos de clase
        /// </summary>
        /// <param name="A"></param>
        public static void programas(int A)
        {
            switch (A)
                {
                    case 1:
                    ///Programa que llene un arreglo con los numeros enteros comprendidos entre 4 y 14
                    Console.WriteLine("Datos archivados");
                    /// (a) nos archivara los numeros entre 4 y 14
                    int[] a = new int[15];
                        for (int i=4; i <= 14; i++)
                        {
                            a[i] = i;
                            Console.WriteLine(i);
                        }
                        Console.ReadKey();
                        break;
                case 2:
                    ///Programa que llene un arreglo con los números pares comprendidos entre 1 y 100
                    Console.WriteLine("Datos pares");
                    int[] b = new int[101];
                    /// (b) nos guarda todos los numeros del 1 al 100
                    for (int i=0; i<= 100; i++)
                    {
                        b[i] = i;
                        ///Se creara un if que nos permitira unicamente escribir los numeros pares
                        ///que se encuentren entre 1 y 100
                        if (b[i] % 2==0)
                        {
                            Console.WriteLine(i);
                        }
                    }
                    Console.ReadKey();
                    break;
                case 3:
                    ///Programa que llene un arreglo con los números comprendidos entre 0 y 100 divisibles por 3
                    Console.WriteLine("Numeros divisibles entre 3");
                    int[] c = new int[101];
                    /// (c) nos guarda los numeros del 1 al 100
                    for(int i = 0; i <= 100; i++)
                    {
                        c[i] = i;
                        ///Se crea un if para encontrar todos los multiplos de 3 y escribirlos
                        if(c[i] % 3 == 0)
                        {
                            Console.WriteLine(i);
                        }
                    }
                    Console.ReadKey();
                    break;
                }
        }
    }
}
