﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Console.WriteLine("Menu de los ejercicios en clase");
            Console.WriteLine("Las opciones disponibles son: \n" +
                "1) Escribir un programa que llene un arreglo con los números enteros comprendidos entre 4 y 14. \n" +
                "2) Escribir un programa que llene un arreglo con los números pares comprendidos entre 1 y 100. \n" +
                "3) Escribir un programa que llene un arreglo con los números comprendidos entre 0 y 100 divisibles por 3.");
            int A;
            Console.WriteLine("\n" +
                "Anota la opcion que deseas");
            ///A va a guardar la opcion elegida del menu principal
            A = int.Parse(Console.ReadLine());
            if (A >= 1 && A <= 9)
            {
                ejercicio.programas(A);
            }
            else
            {
                Console.WriteLine("La opcion no es permitadda favor de agregar otra variable \n" +
                    "Anota otra opcion");
                do
                {
                    A = int.Parse(Console.ReadLine());
                } while (A <= 1 && A >= 9);
                ejercicio.programas(A);
            }
        }
    }
}
